package business;

import java.util.List;

import dao.OrderDAO;
import models.Order;

public class OrderService {
	private OrderDAO orderDAO;
	
	public OrderService() {
		orderDAO = new OrderDAO();
	}
	
	public List<Order> selectAllOrders() {
		return orderDAO.selectAllQuery();
	}
	
	public Order selectByID(int ID) {
		return orderDAO.selectByIDQuery(ID);
	}
	
	public void insertOrder(Order order) {
		orderDAO.insertQuery(order);
	}
	
	public void updateOrder(Order order) {
		orderDAO.updateByIDQuery(order);
	}
	
	public void deleteOrder(Order order) {
		orderDAO.deleteByIDQuery(order);
	}
}
