package business;

import java.util.List;

import dao.ClientDAO;
import models.Client;

public class ClientService {
	private ClientDAO clientDAO;
	
	/**
	 * Contrustor the ClientService class, where the ClientDAO is initialized
	 * 
	 */
	public ClientService() {
		clientDAO = new ClientDAO();
	}
	
	/**
	 * Returns all the clients from the database using the clientDAO
	 * @return a List of all clients from the database
	 */
	public List<Client> selectAllClients() {
		return clientDAO.selectAllQuery();
	}
	
	/**
	 * Returns the client with a specific ID from the database using the clientDAO
	 * @param ID - unique ID for Client
	 * @return a client from the database
	 */
	public Client selectByID(int ID) {
		return clientDAO.selectByIDQuery(ID);
	}
	
	/**
	 * Inserts into the database the client given as parameter
	 * @param client - the client which will be inserted
	 */
	public void insertClient(Client client) {
		clientDAO.insertQuery(client);
	}
	
	/**
	 * Updates from the database the client given as parameter with a specific ID
	 * @param client - the client which will be updated
	 */
	public void updateClient(Client client) {
		clientDAO.updateByIDQuery(client);
	}
	
	/**
	 * Deletes from the database the client given as parameter
	 * @param client - the client which will be deleted
	 */
	public void deleteClient(Client client) {
		clientDAO.deleteByIDQuery(client);
	}
}
