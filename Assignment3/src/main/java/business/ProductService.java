package business;

import java.util.List;

import dao.ProductDAO;
import models.Product;

public class ProductService {
	private ProductDAO productDAO;

	public ProductService() {
		productDAO = new ProductDAO();
	}
	
	public List<Product> selectAllProducts() {
		return productDAO.selectAllQuery();
	}
	
	public Product selectByID(int ID) {
		return productDAO.selectByIDQuery(ID);
	}
	
	public void insertProduct(Product product) {
		productDAO.insertQuery(product);
	}
	
	public void updateProduct(Product product) {
		productDAO.updateByIDQuery(product);
	}
	
	public void deleteProduct(Product product) {
		productDAO.deleteByIDQuery(product);
	}
}
