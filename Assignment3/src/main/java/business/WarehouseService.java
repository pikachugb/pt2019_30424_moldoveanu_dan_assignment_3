package business;

import java.util.List;

import dao.WarehouseDAO;
import models.Warehouse;

public class WarehouseService {
	private WarehouseDAO warehouseDAO;
	
	public WarehouseService() {
		warehouseDAO = new WarehouseDAO();
	}
	
	public List<Warehouse> selectAllWarehouses() {
		return warehouseDAO.selectAllQuery();
	}
	
	public Warehouse selectByID(int ID) {
		return warehouseDAO.selectByIDQuery(ID);
	}
	
	public void insertWarehouse(Warehouse warehouse) {
		warehouseDAO.insertQuery(warehouse);
	}
	
	public void updateWarehouse(Warehouse warehouse) {
		warehouseDAO.updateByIDQuery(warehouse);
	}
	
	public void deleteWarehouse(Warehouse warehouse) {
		warehouseDAO.deleteByIDQuery(warehouse);
	}
	
	public Warehouse selectByProductIDWarehouse(int ID) {
		return warehouseDAO.selectByProductIDQuery(ID);
	}
}
