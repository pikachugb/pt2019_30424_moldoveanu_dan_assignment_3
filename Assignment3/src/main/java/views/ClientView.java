package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import business.ClientService;
import models.Client;
import presentations.ClientTable;

public class ClientView extends JPanel{
	
	private static final long serialVersionUID = 1L;
    private GridBagConstraints c = new GridBagConstraints();
    //db
	private ClientService clientService = new ClientService();
	private List<Client> list;
    //table panel
	private JPanel scrollPanePanel = new JPanel();
	private JScrollPane scrollPane;
	private ClientTable clientTable = new ClientTable();
	//crud buttons panel
	private JPanel buttonsPanel = new JPanel();
	private JButton insertButton = new JButton("Insert");
	private JButton updateButton = new JButton("Update");
	private JButton deleteButton = new JButton("Delete");
	//third panel
	private JPanel updatePanel = new JPanel();
	//labels+fields
	private JLabel clientNameLabel = new JLabel("Name");
	private JTextField clientNameField = new JTextField();
	private JLabel clientAddressLabel = new JLabel("Address");
	private JTextField clientAddressField = new JTextField();
	private JLabel clientEmailLabel = new JLabel("Email");
	private JTextField clientEmailField = new JTextField();
	//insert panel
	private JButton performInsertClientButton = new JButton("Insert Client");
	//update panel
	private JLabel 	clientIDLabel = new JLabel("Select client");
	private JComboBox<Client> clients = new JComboBox<Client>();
	private JButton performUpdateClientButton = new JButton("Update Client");	
	//delete panel
	private JButton performDeleteClientButton = new JButton("Delete Client");
	//actionlistener for backend
	private ActionListener actionListener;	

	/**
	 * Refresh the JPanel with Clients that contains the table
	 * 
	 */
	public void refreshPanel() { 
		removeJTable();
		generateJTable();
		populateComboBox();
	}

	/**
	 * Removes the JTable with Clients from the JPanel
	 * 
	 */
	public void removeJTable() {
		scrollPanePanel.remove(scrollPane);
		scrollPane = null;
		clientTable.clearTable();
	}
	

	/**
	 * Generates the JTable with Clients and adds it to the frame
	 * 
	 */
	public void generateJTable() {
		scrollPane = new JScrollPane(clientTable);
        scrollPane.setPreferredSize(new Dimension(450, 200));
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        clientTable.createTable(clientService.selectAllClients());
        scrollPanePanel.add(scrollPane);
        scrollPanePanel.setBackground(Color.GREEN);
        this.add(scrollPanePanel,c);
	}
	

	/**
	 * Generates the three buttons for the operations insert, update and delete
	 * 
	 */
	private void generateButtonsPanel() {
		insertButton.setPreferredSize(new Dimension(100,40));
		updateButton.setPreferredSize(new Dimension(100,40));
		deleteButton.setPreferredSize(new Dimension(100,40));
		
		buttonsPanel.setLayout(new GridBagLayout());
		c.insets = new Insets(20,20,20,20);
        c.fill = GridBagConstraints.NONE;
		buttonsPanel.add(insertButton,c);
		insertButton.addActionListener(hidingActionListener);
		c.gridx = 1;
		buttonsPanel.add(updateButton,c);
		updateButton.addActionListener(hidingActionListener);
		c.gridx = 2;
		buttonsPanel.add(deleteButton,c);
		deleteButton.addActionListener(hidingActionListener);
		
		buttonsPanel.setBackground(Color.PINK);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
		this.add(buttonsPanel,c);
	}
	
	/**
	 * Generates the panel with all the TextFields, Combobox, Labels and perform operation buttons
	 * 
	 */
	private void generateUpdatePanel() {
		updatePanel.setLayout(new GridBagLayout());
		c.insets = new Insets(5,5,5,5);
		
		c.gridx = 0;
		c.gridy = 0;
        c.fill = GridBagConstraints.EAST;
        updatePanel.add(clientIDLabel,c);

		c.gridx = 0;
		c.gridy = 1;
		updatePanel.add(clientNameLabel,c);

		c.gridx = 0;
		c.gridy = 2;
		updatePanel.add(clientAddressLabel,c);

		c.gridx = 0;
		c.gridy = 3;
		updatePanel.add(clientEmailLabel,c);
		
		c.gridx = 1;
		c.gridy = 0;
		clients.setPreferredSize(new Dimension(200,20));
		updatePanel.add(clients,c);
		clients.addActionListener(comboboxActionListener);
		
		c.gridx = 1;
		c.gridy = 1;
		clientNameField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(clientNameField,c);
		
		c.gridx = 1;
		c.gridy = 2;
		clientAddressField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(clientAddressField,c);
		
		c.gridx = 1;
		c.gridy = 3;
		clientEmailField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(clientEmailField,c);
		
		c.gridx = 1;
		c.gridy = 4;
		performInsertClientButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performInsertClientButton,c);
		performInsertClientButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performUpdateClientButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performUpdateClientButton,c);
		performUpdateClientButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performDeleteClientButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performDeleteClientButton,c);
		performDeleteClientButton.addActionListener(actionListener);

		updatePanel.setBackground(Color.CYAN);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;
		this.add(updatePanel,c);
	}

	/**
	 * Populates the combobox with data from the database
	 * 
	 */
	public void populateComboBox() {
		clients.removeAllItems();
		list = clientService.selectAllClients();
		for (Client client: list) {
			clients.addItem(client);
		}
	}

	/**
	 * Contrustor the ClientView class
	 * @param actionListener - ActionListener for the View class
	 */
	public ClientView(ActionListener actionListener) {
		this.actionListener = actionListener;
        this.setLayout(new GridBagLayout());
        updatePanel.setVisible(false);
        generateJTable();
        generateButtonsPanel();
        generateUpdatePanel();
        populateComboBox();
	}

	private ActionListener hidingActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			updatePanel.setVisible(true);
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == insertButton) {
				clients.setVisible(false);
				performInsertClientButton.setVisible(true);
				performUpdateClientButton.setVisible(false);
				performDeleteClientButton.setVisible(false);
				
				clientIDLabel.setVisible(false);
				clientNameLabel.setVisible(true);
				clientNameField.setVisible(true);
				clientAddressLabel.setVisible(true);
				clientAddressField.setVisible(true);
				clientEmailLabel.setVisible(true);
				clientEmailField.setVisible(true);
				clientNameField.setText("");
				clientAddressField.setText("");
				clientEmailField.setText("");
			}
			//update
			if (sourceObject == updateButton) {
				clients.setVisible(true);
				performInsertClientButton.setVisible(false);
				performUpdateClientButton.setVisible(true);
				performDeleteClientButton.setVisible(false);
				
				clientIDLabel.setVisible(true);
				clientNameLabel.setVisible(true);
				clientNameField.setVisible(true);
				clientAddressLabel.setVisible(true);
				clientAddressField.setVisible(true);
				clientEmailLabel.setVisible(true);
				clientEmailField.setVisible(true);

				comboboxActionListener.actionPerformed(e);
			}
			//delete
			if (sourceObject == deleteButton) {
				clients.setVisible(true);
				performInsertClientButton.setVisible(false);
				performUpdateClientButton.setVisible(false);
				performDeleteClientButton.setVisible(true);
				
				clientIDLabel.setVisible(false);
				clientNameLabel.setVisible(false);
				clientNameField.setVisible(false);
				clientAddressLabel.setVisible(false);
				clientAddressField.setVisible(false);
				clientEmailLabel.setVisible(false);
				clientEmailField.setVisible(false);
			}
		}
	};
	
	private ActionListener comboboxActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			//combobox
			if (performUpdateClientButton.isVisible() && clients.getItemCount() != 0) {
				Client client = (Client) clients.getSelectedItem();
				clientNameField.setText(client.getName());
				clientAddressField.setText(client.getAddress());
				clientEmailField.setText(client.getEmail());
			}			
		}
	};

	public JButton getPerformInsertClientButton() {
		return performInsertClientButton;
	}

	public JButton getPerformUpdateClientButton() {
		return performUpdateClientButton;
	}

	public JButton getPerformDeleteClientButton() {
		return performDeleteClientButton;
	}

	public JTextField getClientNameField() {
		return clientNameField;
	}

	public JTextField getClientAddressField() {
		return clientAddressField;
	}

	public JTextField getClientEmailField() {
		return clientEmailField;
	}

	public JComboBox<Client> getClients() {
		return clients;
	}
	
	public ClientTable getClientTable() {
		return clientTable;
	}
}
