package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import business.ProductService;
import models.Product;
import presentations.ProductTable;

public class ProductView extends JPanel{
	
	private static final long serialVersionUID = 1L;
    private GridBagConstraints c = new GridBagConstraints();
    //db
	private ProductService productService = new ProductService();
	private List<Product> list;
    //table panel
	private JPanel scrollPanePanel = new JPanel();
	private JScrollPane scrollPane;
	private ProductTable productTable = new ProductTable();
	//crud buttons panel
	private JPanel buttonsPanel = new JPanel();
	private JButton insertButton = new JButton("Insert");
	private JButton updateButton = new JButton("Update");
	private JButton deleteButton = new JButton("Delete");
	//third panel
	private JPanel updatePanel = new JPanel();
	//labels+fields
	private JLabel productNameLabel = new JLabel("Name");
	private JTextField productNameField = new JTextField();
	private JLabel productPriceLabel = new JLabel("Price");
	private JTextField productPriceField = new JTextField();
	//insert panel
	private JButton performInsertProductButton = new JButton("Insert Product");
	//update panel
	private JLabel 	productIDLabel = new JLabel("Select product");
	private JComboBox<Product> products = new JComboBox<Product>();
	private JButton performUpdateProductButton = new JButton("Update Product");	
	//delete panel
	private JButton performDeleteProductButton = new JButton("Delete Product");
	//actionlistener for backend
	private ActionListener actionListener;
	
	public ProductTable getProductTable() {
		return productTable;
	}
	
	public void refreshPanel() { 
		removeJTable();
		generateJTable();
		populateComboBox();
	}

	public void removeJTable() {
		scrollPanePanel.remove(scrollPane);
		scrollPane = null;
		productTable.clearTable();
	}
	
	public void generateJTable() {
		scrollPane = new JScrollPane(productTable);
        scrollPane.setPreferredSize(new Dimension(450, 200));
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        productTable.createTable(productService.selectAllProducts());
        scrollPanePanel.add(scrollPane);
        scrollPanePanel.setBackground(Color.GREEN);
        this.add(scrollPanePanel,c);
	}
	
	private void generateButtonsPanel() {
		insertButton.setPreferredSize(new Dimension(100,40));
		updateButton.setPreferredSize(new Dimension(100,40));
		deleteButton.setPreferredSize(new Dimension(100,40));
		
		buttonsPanel.setLayout(new GridBagLayout());
		c.insets = new Insets(20,20,20,20);
        c.fill = GridBagConstraints.NONE;
		buttonsPanel.add(insertButton,c);
		insertButton.addActionListener(hidingActionListener);
		c.gridx = 1;
		buttonsPanel.add(updateButton,c);
		updateButton.addActionListener(hidingActionListener);
		c.gridx = 2;
		buttonsPanel.add(deleteButton,c);
		deleteButton.addActionListener(hidingActionListener);
		
		buttonsPanel.setBackground(Color.PINK);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
		this.add(buttonsPanel,c);
	}
	
	private void generateUpdatePanel() {
		updatePanel.setLayout(new GridBagLayout());
		c.insets = new Insets(5,5,5,5);
		
		c.gridx = 0;
		c.gridy = 0;
        c.fill = GridBagConstraints.EAST;
        updatePanel.add(productIDLabel,c);

		c.gridx = 0;
		c.gridy = 1;
		updatePanel.add(productNameLabel,c);

		c.gridx = 0;
		c.gridy = 2;
		updatePanel.add(productPriceLabel,c);
		
		c.gridx = 1;
		c.gridy = 0;
		products.setPreferredSize(new Dimension(200,20));
		updatePanel.add(products,c);
		products.addActionListener(comboboxActionListener);
		
		c.gridx = 1;
		c.gridy = 1;
		productNameField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(productNameField,c);
		
		c.gridx = 1;
		c.gridy = 2;
		productPriceField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(productPriceField,c);
		
		c.gridx = 1;
		c.gridy = 4;
		performInsertProductButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performInsertProductButton,c);
		performInsertProductButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performUpdateProductButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performUpdateProductButton,c);
		performUpdateProductButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performDeleteProductButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performDeleteProductButton,c);
		performDeleteProductButton.addActionListener(actionListener);

		updatePanel.setBackground(Color.CYAN);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;
		this.add(updatePanel,c);
	}
	
	public void populateComboBox() {
		products.removeAllItems();
		list = productService.selectAllProducts();
		for (Product product: list) {
			products.addItem(product);
		}
	}

	public ProductView(ActionListener actionListener) {
		this.actionListener = actionListener;
        this.setLayout(new GridBagLayout());
        updatePanel.setVisible(false);
        generateJTable();
        generateButtonsPanel();
        generateUpdatePanel();
        populateComboBox();
	}
	
	//actionlistener for frontend
	private ActionListener hidingActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			updatePanel.setVisible(true);
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == insertButton) {
				products.setVisible(false);
				performInsertProductButton.setVisible(true);
				performUpdateProductButton.setVisible(false);
				performDeleteProductButton.setVisible(false);
				
				productIDLabel.setVisible(false);
				productNameLabel.setVisible(true);
				productNameField.setVisible(true);
				productPriceLabel.setVisible(true);
				productPriceField.setVisible(true);
				productNameField.setText("");
				productPriceField.setText("");
			}
			//update
			if (sourceObject == updateButton) {
				products.setVisible(true);
				performInsertProductButton.setVisible(false);
				performUpdateProductButton.setVisible(true);
				performDeleteProductButton.setVisible(false);
				
				productIDLabel.setVisible(true);
				productNameLabel.setVisible(true);
				productNameField.setVisible(true);
				productPriceLabel.setVisible(true);
				productPriceField.setVisible(true);

				comboboxActionListener.actionPerformed(e);
			}
			//delete
			if (sourceObject == deleteButton) {
				products.setVisible(true);
				performInsertProductButton.setVisible(false);
				performUpdateProductButton.setVisible(false);
				performDeleteProductButton.setVisible(true);
				
				productIDLabel.setVisible(false);
				productNameLabel.setVisible(false);
				productNameField.setVisible(false);
				productPriceLabel.setVisible(false);
				productPriceField.setVisible(false);
			}
		}
	};
	
	private ActionListener comboboxActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			//combobox
			if (performUpdateProductButton.isVisible() && products.getItemCount() != 0) {
				Product product = (Product) products.getSelectedItem();
				productNameField.setText(product.getName());
				productPriceField.setText(String.valueOf(product.getPrice()));
			}			
		}
	};

	public JButton getPerformInsertProductButton() {
		return performInsertProductButton;
	}

	public JButton getPerformUpdateProductButton() {
		return performUpdateProductButton;
	}

	public JButton getPerformDeleteProductButton() {
		return performDeleteProductButton;
	}

	public JTextField getProductNameField() {
		return productNameField;
	}

	public JTextField getProductPriceField() {
		return productPriceField;
	}

	public JComboBox<Product> getProducts() {
		return products;
	}
}
