package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import business.OrderService;
import models.Order;
import presentations.OrderTable;

public class OrderView extends JPanel{
	
	private static final long serialVersionUID = 1L;
    private GridBagConstraints c = new GridBagConstraints();
    //db
	private OrderService orderService = new OrderService();
	private List<Order> list;
    //table panel
	private JPanel scrollPanePanel = new JPanel();
	private JScrollPane scrollPane;
	private OrderTable orderTable = new OrderTable();
	//crud buttons panel
	private JPanel buttonsPanel = new JPanel();
	private JButton insertButton = new JButton("New Order");
	private JButton updateButton = new JButton("Update");
	private JButton deleteButton = new JButton("Delete");
	//third panel
	private JPanel updatePanel = new JPanel();
	//labels+fields
	private JLabel orderClientIDLabel = new JLabel("ClientID");
	private JTextField orderClientIDField = new JTextField();
	private JLabel orderProductIDLabel = new JLabel("ProductID");
	private JTextField orderProductIDField = new JTextField();
	private JLabel orderQuantityLabel = new JLabel("Quantity");
	private JTextField orderQuantityField = new JTextField();
	//insert panel
	private JButton performInsertOrderButton = new JButton("Place Order");
	//update panel
	private JLabel 	orderIDLabel = new JLabel("Select order");
	private JComboBox<Order> orders = new JComboBox<Order>();
	private JButton performUpdateOrderButton = new JButton("Update Order");	
	//delete panel
	private JButton performDeleteOrderButton = new JButton("Delete Order");
	//actionlistener for backend
	private ActionListener actionListener;
	
	public OrderTable getOrderTable() {
		return orderTable;
	}
	
	public void refreshPanel() { 
		removeJTable();
		generateJTable();
		populateComboBox();
	}

	public void removeJTable() {
		scrollPanePanel.remove(scrollPane);
		scrollPane = null;
		orderTable.clearTable();
	}
	
	public void generateJTable() {
		scrollPane = new JScrollPane(orderTable);
        scrollPane.setPreferredSize(new Dimension(450, 200));
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        orderTable.createTable(orderService.selectAllOrders());
        scrollPanePanel.add(scrollPane);
        scrollPanePanel.setBackground(Color.GREEN);
        this.add(scrollPanePanel,c);
	}
	
	private void generateButtonsPanel() {
		insertButton.setPreferredSize(new Dimension(100,40));
		updateButton.setPreferredSize(new Dimension(100,40));
		deleteButton.setPreferredSize(new Dimension(100,40));
		
		buttonsPanel.setLayout(new GridBagLayout());
		c.insets = new Insets(20,20,20,20);
        c.fill = GridBagConstraints.NONE;
		buttonsPanel.add(insertButton,c);
		insertButton.addActionListener(hidingActionListener);
		c.gridx = 1;
		buttonsPanel.add(updateButton,c);
		updateButton.addActionListener(hidingActionListener);
		c.gridx = 2;
		buttonsPanel.add(deleteButton,c);
		deleteButton.addActionListener(hidingActionListener);
		
		buttonsPanel.setBackground(Color.PINK);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
		this.add(buttonsPanel,c);
	}
	
	private void generateUpdatePanel() {
		updatePanel.setLayout(new GridBagLayout());
		c.insets = new Insets(5,5,5,5);
		
		c.gridx = 0;
		c.gridy = 0;
        c.fill = GridBagConstraints.EAST;
        updatePanel.add(orderIDLabel,c);

		c.gridx = 0;
		c.gridy = 1;
		updatePanel.add(orderClientIDLabel,c);

		c.gridx = 0;
		c.gridy = 2;
		updatePanel.add(orderProductIDLabel,c);

		c.gridx = 0;
		c.gridy = 3;
		updatePanel.add(orderQuantityLabel,c);
		
		c.gridx = 1;
		c.gridy = 0;
		orders.setPreferredSize(new Dimension(200,20));
		updatePanel.add(orders,c);
		orders.addActionListener(comboboxActionListener);
		
		c.gridx = 1;
		c.gridy = 1;
		orderClientIDField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(orderClientIDField,c);
		
		c.gridx = 1;
		c.gridy = 2;
		orderProductIDField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(orderProductIDField,c);
		
		c.gridx = 1;
		c.gridy = 3;
		orderQuantityField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(orderQuantityField,c);
		
		c.gridx = 1;
		c.gridy = 4;
		performInsertOrderButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performInsertOrderButton,c);
		performInsertOrderButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performUpdateOrderButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performUpdateOrderButton,c);
		performUpdateOrderButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performDeleteOrderButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performDeleteOrderButton,c);
		performDeleteOrderButton.addActionListener(actionListener);

		updatePanel.setBackground(Color.CYAN);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;
		this.add(updatePanel,c);
	}
	
	public void populateComboBox() {
		orders.removeAllItems();
		list = orderService.selectAllOrders();
		for (Order order: list) {
			orders.addItem(order);
		}
	}

	public OrderView(ActionListener actionListener) {
		this.actionListener = actionListener;
        this.setLayout(new GridBagLayout());
        updatePanel.setVisible(false);
        generateJTable();
        generateButtonsPanel();
        generateUpdatePanel();
        populateComboBox();
	}
	
	//actionlistener for frontend
	private ActionListener hidingActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			updatePanel.setVisible(true);
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == insertButton) {
				orders.setVisible(false);
				performInsertOrderButton.setVisible(true);
				performUpdateOrderButton.setVisible(false);
				performDeleteOrderButton.setVisible(false);
				
				orderIDLabel.setVisible(false);
				orderClientIDLabel.setVisible(true);
				orderClientIDField.setVisible(true);
				orderProductIDLabel.setVisible(true);
				orderProductIDField.setVisible(true);
				orderQuantityLabel.setVisible(true);
				orderQuantityField.setVisible(true);
				orderClientIDField.setText("");
				orderProductIDField.setText("");
				orderQuantityField.setText("");
			}
			//update
			if (sourceObject == updateButton) {
				orders.setVisible(true);
				performInsertOrderButton.setVisible(false);
				performUpdateOrderButton.setVisible(true);
				performDeleteOrderButton.setVisible(false);
				
				orderIDLabel.setVisible(true);
				orderClientIDLabel.setVisible(true);
				orderClientIDField.setVisible(true);
				orderProductIDLabel.setVisible(true);
				orderProductIDField.setVisible(true);
				orderQuantityLabel.setVisible(true);
				orderQuantityField.setVisible(true);

				comboboxActionListener.actionPerformed(e);
			}
			//delete
			if (sourceObject == deleteButton) {
				orders.setVisible(true);
				performInsertOrderButton.setVisible(false);
				performUpdateOrderButton.setVisible(false);
				performDeleteOrderButton.setVisible(true);
				
				orderIDLabel.setVisible(false);
				orderClientIDLabel.setVisible(false);
				orderClientIDField.setVisible(false);
				orderProductIDLabel.setVisible(false);
				orderProductIDField.setVisible(false);
				orderQuantityLabel.setVisible(false);
				orderQuantityField.setVisible(false);
			}
		}
	};
	
	private ActionListener comboboxActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			//combobox
			if (performUpdateOrderButton.isVisible() && orders.getItemCount() != 0) {
				Order order = (Order) orders.getSelectedItem();
				orderClientIDField.setText(String.valueOf(order.getClientID()));
				orderProductIDField.setText(String.valueOf(order.getProductID()));
				orderQuantityField.setText(String.valueOf(order.getQuantity()));
			}			
		}
	};

	public JButton getPerformInsertOrderButton() {
		return performInsertOrderButton;
	}

	public JButton getPerformUpdateOrderButton() {
		return performUpdateOrderButton;
	}

	public JButton getPerformDeleteOrderButton() {
		return performDeleteOrderButton;
	}

	public JTextField getOrderClientIDField() {
		return orderClientIDField;
	}

	public JTextField getOrderProductIDField() {
		return orderProductIDField;
	}

	public JTextField getOrderQuantityField() {
		return orderQuantityField;
	}

	public JComboBox<Order> getOrders() {
		return orders;
	}
}
