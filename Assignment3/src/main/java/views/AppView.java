package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class AppView extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabbedPane;
	
	/**
	 * Constructor for the AppView class which adds the tabs and sets the size for the frame
	 * @param p1 - tab1
	 * @param p2 - tab2
	 * @param p3 - tab3 
	 * @param p4 - tab4
	 */
	public AppView(JPanel p1, JPanel p2, JPanel p3, JPanel p4) {
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Client", null, p1, null);
		tabbedPane.addTab("Product", null, p2, null);
		tabbedPane.addTab("Warehouse", null, p3, null);
		tabbedPane.addTab("Order", null, p4, null);
		this.add(tabbedPane);
		
		this.setTitle("Order Management");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
}
