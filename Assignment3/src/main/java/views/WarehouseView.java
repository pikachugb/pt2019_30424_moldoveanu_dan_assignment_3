package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import business.WarehouseService;
import models.Warehouse;
import presentations.WarehouseTable;

public class WarehouseView extends JPanel{
	
	private static final long serialVersionUID = 1L;
    private GridBagConstraints c = new GridBagConstraints();
    //db
	private WarehouseService warehouseService = new WarehouseService();
	private List<Warehouse> list;
    //table panel
	private JPanel scrollPanePanel = new JPanel();
	private JScrollPane scrollPane;
	private WarehouseTable warehouseTable = new WarehouseTable();
	//crud buttons panel
	private JPanel buttonsPanel = new JPanel();
	private JButton insertButton = new JButton("Insert");
	private JButton updateButton = new JButton("Update");
	private JButton deleteButton = new JButton("Delete");
	//third panel
	private JPanel updatePanel = new JPanel();
	//labels+fields
	private JLabel warehouseProductIDLabel = new JLabel("ProductID");
	private JTextField warehouseProductIDField = new JTextField();
	private JLabel warehouseQuantityLabel = new JLabel("Quantity");
	private JTextField warehouseQuantityField = new JTextField();
	//insert panel
	private JButton performInsertWarehouseButton = new JButton("Insert Warehouse");
	//update panel
	private JLabel 	warehouseIDLabel = new JLabel("Select warehouse");
	private JComboBox<Warehouse> warehouses = new JComboBox<Warehouse>();
	private JButton performUpdateWarehouseButton = new JButton("Update Warehouse");	
	//delete panel
	private JButton performDeleteWarehouseButton = new JButton("Delete Warehouse");
	//actionlistener for backend
	private ActionListener actionListener;
	
	public WarehouseTable getWarehouseTable() {
		return warehouseTable;
	}
	
	public void refreshPanel() { 
		removeJTable();
		generateJTable();
		populateComboBox();
	}

	public void removeJTable() {
		scrollPanePanel.remove(scrollPane);
		scrollPane = null;
		warehouseTable.clearTable();
	}
	
	public void generateJTable() {
		scrollPane = new JScrollPane(warehouseTable);
        scrollPane.setPreferredSize(new Dimension(450, 200));
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        warehouseTable.createTable(warehouseService.selectAllWarehouses());
        scrollPanePanel.add(scrollPane);
        scrollPanePanel.setBackground(Color.GREEN);
        this.add(scrollPanePanel,c);
	}
	
	private void generateButtonsPanel() {
		insertButton.setPreferredSize(new Dimension(100,40));
		updateButton.setPreferredSize(new Dimension(100,40));
		deleteButton.setPreferredSize(new Dimension(100,40));
		
		buttonsPanel.setLayout(new GridBagLayout());
		c.insets = new Insets(20,20,20,20);
        c.fill = GridBagConstraints.NONE;
		buttonsPanel.add(insertButton,c);
		insertButton.addActionListener(hidingActionListener);
		c.gridx = 1;
		buttonsPanel.add(updateButton,c);
		updateButton.addActionListener(hidingActionListener);
		c.gridx = 2;
		buttonsPanel.add(deleteButton,c);
		deleteButton.addActionListener(hidingActionListener);
		
		buttonsPanel.setBackground(Color.PINK);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
		this.add(buttonsPanel,c);
	}
	
	private void generateUpdatePanel() {
		updatePanel.setLayout(new GridBagLayout());
		c.insets = new Insets(5,5,5,5);
		
		c.gridx = 0;
		c.gridy = 0;
        c.fill = GridBagConstraints.EAST;
        updatePanel.add(warehouseIDLabel,c);

		c.gridx = 0;
		c.gridy = 1;
		updatePanel.add(warehouseProductIDLabel,c);

		c.gridx = 0;
		c.gridy = 2;
		updatePanel.add(warehouseQuantityLabel,c);
		
		c.gridx = 1;
		c.gridy = 0;
		warehouses.setPreferredSize(new Dimension(200,20));
		updatePanel.add(warehouses,c);
		warehouses.addActionListener(comboboxActionListener);
		
		c.gridx = 1;
		c.gridy = 1;
		warehouseProductIDField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(warehouseProductIDField,c);
		
		c.gridx = 1;
		c.gridy = 2;
		warehouseQuantityField.setPreferredSize(new Dimension(200,20));
		updatePanel.add(warehouseQuantityField,c);
		
		c.gridx = 1;
		c.gridy = 4;
		performInsertWarehouseButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performInsertWarehouseButton,c);
		performInsertWarehouseButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performUpdateWarehouseButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performUpdateWarehouseButton,c);
		performUpdateWarehouseButton.addActionListener(actionListener);
		
		c.gridx = 1;
		c.gridy = 4;
		performDeleteWarehouseButton.setPreferredSize(new Dimension(150,30));
		updatePanel.add(performDeleteWarehouseButton,c);
		performDeleteWarehouseButton.addActionListener(actionListener);

		updatePanel.setBackground(Color.CYAN);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;
		this.add(updatePanel,c);
	}
	
	public void populateComboBox() {
		warehouses.removeAllItems();
		list = warehouseService.selectAllWarehouses();
		for (Warehouse warehouse: list) {
			warehouses.addItem(warehouse);
		}
	}

	public WarehouseView(ActionListener actionListener) {
		this.actionListener = actionListener;
        this.setLayout(new GridBagLayout());
        updatePanel.setVisible(false);
        generateJTable();
        generateButtonsPanel();
        generateUpdatePanel();
        populateComboBox();
	}
	
	//actionlistener for frontend
	private ActionListener hidingActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			updatePanel.setVisible(true);
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == insertButton) {
				warehouses.setVisible(false);
				performInsertWarehouseButton.setVisible(true);
				performUpdateWarehouseButton.setVisible(false);
				performDeleteWarehouseButton.setVisible(false);
				
				warehouseIDLabel.setVisible(false);
				warehouseProductIDLabel.setVisible(true);
				warehouseProductIDField.setVisible(true);
				warehouseQuantityLabel.setVisible(true);
				warehouseQuantityField.setVisible(true);
				warehouseProductIDField.setText("");
				warehouseQuantityField.setText("");
			}
			//update
			if (sourceObject == updateButton) {
				warehouses.setVisible(true);
				performInsertWarehouseButton.setVisible(false);
				performUpdateWarehouseButton.setVisible(true);
				performDeleteWarehouseButton.setVisible(false);
				
				warehouseIDLabel.setVisible(true);
				warehouseProductIDLabel.setVisible(true);
				warehouseProductIDField.setVisible(true);
				warehouseQuantityLabel.setVisible(true);
				warehouseQuantityField.setVisible(true);

				comboboxActionListener.actionPerformed(e);
			}
			//delete
			if (sourceObject == deleteButton) {
				warehouses.setVisible(true);
				performInsertWarehouseButton.setVisible(false);
				performUpdateWarehouseButton.setVisible(false);
				performDeleteWarehouseButton.setVisible(true);
				
				warehouseIDLabel.setVisible(false);
				warehouseProductIDLabel.setVisible(false);
				warehouseProductIDField.setVisible(false);
				warehouseQuantityLabel.setVisible(false);
				warehouseQuantityField.setVisible(false);
			}
		}
	};
	
	private ActionListener comboboxActionListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			//combobox
			if (performUpdateWarehouseButton.isVisible() && warehouses.getItemCount() != 0) {
				Warehouse warehouse = (Warehouse) warehouses.getSelectedItem();
				warehouseProductIDField.setText(String.valueOf(warehouse.getProductID()));
				warehouseQuantityField.setText(String.valueOf(warehouse.getQuantity()));
			}			
		}
	};

	public JButton getPerformInsertWarehouseButton() {
		return performInsertWarehouseButton;
	}

	public JButton getPerformUpdateWarehouseButton() {
		return performUpdateWarehouseButton;
	}

	public JButton getPerformDeleteWarehouseButton() {
		return performDeleteWarehouseButton;
	}

	public JTextField getWarehouseProductIDField() {
		return warehouseProductIDField;
	}

	public JTextField getWarehouseQuantityField() {
		return warehouseQuantityField;
	}

	public JComboBox<Warehouse> getWarehouses() {
		return warehouses;
	}
}
