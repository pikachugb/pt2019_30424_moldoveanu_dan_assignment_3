package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import business.ClientService;
import business.OrderService;
import business.ProductService;
import business.WarehouseService;
import models.Bill;
import models.Client;
import models.Order;
import models.Product;
import models.Warehouse;
import views.AppView;
import views.ClientView;
import views.OrderView;
import views.ProductView;
import views.WarehouseView;

public class Controller {
	
	private ClientView clientView;
	private ProductView productView;
	private WarehouseView warehouseView;
	private OrderView orderView;
	private AppView appView;
	
	private ClientService clientService = new ClientService();
	private ProductService productService = new ProductService();
	private WarehouseService warehouseService = new WarehouseService();
	private OrderService orderService = new OrderService();
	
	/**
	 * Clear the TextFields on every view.
	 * 
	 */
	private void clearTextFields() {
		clientView.getClientNameField().setText("");
		clientView.getClientAddressField().setText("");
		clientView.getClientEmailField().setText("");

		productView.getProductNameField().setText("");
		productView.getProductPriceField().setText("");
		
		warehouseView.getWarehouseProductIDField().setText("");
		warehouseView.getWarehouseQuantityField().setText("");
		
		orderView.getOrderClientIDField().setText("");
		orderView.getOrderProductIDField().setText("");
		orderView.getOrderQuantityField().setText("");
	}
	
	/**
	 * Checks if the input is correct for the client: all textfields not null & correct email address.
	 * @return true if correct, false if wrong input.
	 */
	private Boolean clientValidators() {
		String name = clientView.getClientNameField().getText();
		String address = clientView.getClientAddressField().getText();
		String email = clientView.getClientEmailField().getText();
		
		if (name.equals("") || address.equals("") || email.equals("")) {
			JOptionPane.showMessageDialog(null, "All fields must be filled!", "Input error", JOptionPane.ERROR_MESSAGE);			
			return false;
		}
		
		if (!email.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{1,4}")) {
			JOptionPane.showMessageDialog(null, "Wrong email format!", "Input error", JOptionPane.WARNING_MESSAGE);				
			return false;
		}
		
		return true;
	}
	
	private ActionListener clientActionListener = new ActionListener() {
		
		/**
		 * Click event for the buttons from the ClientView.
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == clientView.getPerformInsertClientButton() && clientValidators()) {
				String name = clientView.getClientNameField().getText();
				String address = clientView.getClientAddressField().getText();
				String email = clientView.getClientEmailField().getText();
				Client client = new Client(name, address, email);
				
				clientService.insertClient(client);
				clientView.refreshPanel();
				clearTextFields();
			}
			
			//update
			if (sourceObject == clientView.getPerformUpdateClientButton() && clientView.getClients().getItemCount() != 0 && clientValidators()) {
				int id = ((Client) clientView.getClients().getSelectedItem()).getID();
				String name = clientView.getClientNameField().getText();
				String address = clientView.getClientAddressField().getText();
				String email = clientView.getClientEmailField().getText();
				Client client = new Client(id, name, address, email);
				
				clientService.updateClient(client);
				clientView.refreshPanel();
			}
			//delete
			if (sourceObject == clientView.getPerformDeleteClientButton()  && clientView.getClients().getItemCount() != 0) {
				Client client = (Client) clientView.getClients().getSelectedItem();
				clientService.deleteClient(client);
				clientView.refreshPanel();
			}
		}
	};

	/**
	 * Checks if the input is correct for the product: all textfields not null & existing productID & price > 0.
	 * @return true if correct, false if wrong input.
	 */
	private Boolean productValidators() {
		String name = productView.getProductNameField().getText();
		String price = productView.getProductPriceField().getText();
		
		if (name.equals("") || price.equals("")) {
			JOptionPane.showMessageDialog(null, "All fields must be filled!", "Input error", JOptionPane.ERROR_MESSAGE);			
			return false;
		}

		int priceInt = 1;
		try {
			 priceInt = Integer.parseInt(price);
		}
		catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Introduce only numbers for the price!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (priceInt <= 0) {
			JOptionPane.showMessageDialog(null, "Introduce a positive price!", "Input error", JOptionPane.ERROR_MESSAGE);				
			return false;
		}
		
		return true;
	}
	
	private ActionListener productActionListener = new ActionListener() {

		/**
		 * Click event for the buttons from the ProductView.
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == productView.getPerformInsertProductButton() && productValidators()) {
				String name = productView.getProductNameField().getText();
				int price = Integer.parseInt(productView.getProductPriceField().getText());
				Product product = new Product(name, price);
				
				productService.insertProduct(product);
				productView.refreshPanel();
				clearTextFields();
			}
			
			//update
			if (sourceObject == productView.getPerformUpdateProductButton() && productValidators() && productView.getProducts().getItemCount() != 0) {
				int id = ((Product) productView.getProducts().getSelectedItem()).getID();
				String name = productView.getProductNameField().getText();
				int price = Integer.parseInt(productView.getProductPriceField().getText());
				Product product = new Product(id, name, price);
				
				productService.updateProduct(product);
				productView.refreshPanel();
			}
			//delete
			if (sourceObject == productView.getPerformDeleteProductButton() && productView.getProducts().getItemCount() != 0) {
				Product product = (Product) productView.getProducts().getSelectedItem();
				productService.deleteProduct(product);
				productView.refreshPanel();
			}
		}
	};

	/**
	 * Checks if the input is correct for the warehouse: all textfields not null & quantity > 0.
	 * @return true if correct, false if wrong input.
	 */
	private Boolean warehouseValidators() {
		String productID = warehouseView.getWarehouseProductIDField().getText();
		String quantity = warehouseView.getWarehouseQuantityField().getText();
		
		if (productID.equals("") || quantity.equals("")) {
			JOptionPane.showMessageDialog(null, "All fields must be filled!", "Input error", JOptionPane.ERROR_MESSAGE);			
			return false;
		}

		int quantityInt = 1;
		try {
			 quantityInt = Integer.parseInt(quantity);
		}
		catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Introduce only numbers for the quantity!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (quantityInt <= 0) {
			JOptionPane.showMessageDialog(null, "Introduce a positive quantity!", "Input error", JOptionPane.ERROR_MESSAGE);				
			return false;
		}
		
		int productIDInt = 1;

		try {
			 productIDInt = Integer.parseInt(productID);
		}
		catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Introduce only numbers for the productID!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		Product product = productService.selectByID(productIDInt);
		if (product == null) {
			JOptionPane.showMessageDialog(null, "ProductID was not found in the Products table!\n", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;					
		}
		
		Warehouse warehouse = warehouseService.selectByProductIDWarehouse(productIDInt);
		if (warehouseView.getPerformInsertWarehouseButton().isVisible() && warehouse != null) {
			JOptionPane.showMessageDialog(null, "ProductID already exists in the warehouse!\nUse the update section!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;				
		}
		
		return true;
	}
	
	private ActionListener warehouseActionListener = new ActionListener() {

		/**
		 * Click event for the buttons from the WarehouseView.
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == warehouseView.getPerformInsertWarehouseButton() && warehouseValidators()) {
				int name = Integer.parseInt(warehouseView.getWarehouseProductIDField().getText());
				int quantity = Integer.parseInt(warehouseView.getWarehouseQuantityField().getText());
				Warehouse warehouse = new Warehouse(name, quantity);
				
				warehouseService.insertWarehouse(warehouse);
				warehouseView.refreshPanel();
				clearTextFields();
			}
			
			//update
			if (sourceObject == warehouseView.getPerformUpdateWarehouseButton() && warehouseValidators() && warehouseView.getWarehouses().getItemCount() != 0) {
				int id = ((Warehouse) warehouseView.getWarehouses().getSelectedItem()).getID();
				int name = Integer.parseInt(warehouseView.getWarehouseProductIDField().getText());
				int quantity = Integer.parseInt(warehouseView.getWarehouseQuantityField().getText());
				Warehouse warehouse = new Warehouse(id, name, quantity);
				
				warehouseService.updateWarehouse(warehouse);
				warehouseView.refreshPanel();
			}
			//delete
			if (sourceObject == warehouseView.getPerformDeleteWarehouseButton() && warehouseView.getWarehouses().getItemCount() != 0) {
				Warehouse warehouse = (Warehouse) warehouseView.getWarehouses().getSelectedItem();
				warehouseService.deleteWarehouse(warehouse);
				warehouseView.refreshPanel();
			}
		}
	};
	
	/**
	 * Checks if the input is correct for the order: all textfields not null & existing clientID & existing productID & quantity > 0.
	 * @return true if correct, false if wrong input.
	 */
	private Boolean orderValidators() {
		String clientID = orderView.getOrderClientIDField().getText();
		String productID = orderView.getOrderProductIDField().getText();
		String quantity = orderView.getOrderQuantityField().getText();
		
		if (clientID.equals("") || productID.equals("") || quantity.equals("")) {
			JOptionPane.showMessageDialog(null, "All fields must be filled!", "Input error", JOptionPane.ERROR_MESSAGE);			
			return false;
		}
		
		int clientIDInt = 1;
		try {
			 clientIDInt = Integer.parseInt(clientID);
		}
		catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Introduce only numbers for the clientID!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		int productIDInt = 1;
		try {
			 productIDInt = Integer.parseInt(productID);
		}
		catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Introduce only numbers for the productID!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		int quantityInt = 1;
		try {
			 quantityInt = Integer.parseInt(quantity);
		}
		catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Introduce only numbers for the quantity!", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (quantityInt <= 0) {
			JOptionPane.showMessageDialog(null, "Introduce a positive quantity!", "Input error", JOptionPane.ERROR_MESSAGE);				
			return false;
		}
		
		Client client = clientService.selectByID(clientIDInt);
		if(client == null) {
			JOptionPane.showMessageDialog(null, "Client was not found in the Clients table!\n", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;	
		}
		
		Product product = productService.selectByID(productIDInt);
		if (product == null) {
			JOptionPane.showMessageDialog(null, "ProductID was not found in the Products table!\n", "Input error", JOptionPane.ERROR_MESSAGE);
			return false;					
		}
		
		return true;
	}
	
	
	private ActionListener orderActionListener = new ActionListener() {

		/**
		 * Click event for the buttons from the OrderView. It also verifies the understock.
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			JButton sourceObject = (JButton) e.getSource();
			
			//insert
			if (sourceObject == orderView.getPerformInsertOrderButton() && orderValidators()) {
				int clientID = Integer.parseInt(orderView.getOrderClientIDField().getText());
				int productID = Integer.parseInt(orderView.getOrderProductIDField().getText());
				int quantity = Integer.parseInt(orderView.getOrderQuantityField().getText());

				Client currentClient = clientService.selectByID(clientID);
				Product currentProduct = productService.selectByID(productID);
				Warehouse currentWarehouse = warehouseService.selectByProductIDWarehouse(productID);
				
				if (currentWarehouse.getQuantity() == 0)
					JOptionPane.showMessageDialog(null, "There are no units left in stock!", "Not enough quantity", JOptionPane.ERROR_MESSAGE);
				else if (currentWarehouse.getQuantity() < quantity)
					JOptionPane.showMessageDialog(null, "There are only " + currentWarehouse.getQuantity() + " units left in stock!", "Not enough quantity", JOptionPane.WARNING_MESSAGE);
				else {
					//remove from warehouse
					currentWarehouse.setQuantity(currentWarehouse.getQuantity() - quantity);
					warehouseService.updateWarehouse(currentWarehouse);
					warehouseView.refreshPanel();
					//place order
					Order order = new Order(clientID, productID, quantity);
					orderService.insertOrder(order);
					orderView.refreshPanel();
					clearTextFields();
					Bill bill = new Bill(currentClient, currentProduct, quantity);
					bill.printBill();
				}
			}
			
			//update
			if (sourceObject == orderView.getPerformUpdateOrderButton() && orderValidators() && orderView.getOrders().getItemCount() != 0) {
				int id = ((Order) orderView.getOrders().getSelectedItem()).getID();
				int currentQuantityOrdered = ((Order) orderView.getOrders().getSelectedItem()).getQuantity();
				int clientID = Integer.parseInt(orderView.getOrderClientIDField().getText());
				int productID = Integer.parseInt(orderView.getOrderProductIDField().getText());
				int quantity = Integer.parseInt(orderView.getOrderQuantityField().getText());
				
				Client currentClient = clientService.selectByID(clientID);
				Product currentProduct = productService.selectByID(productID);
				Warehouse currentWarehouse = warehouseService.selectByProductIDWarehouse(productID);
				
				if (currentWarehouse.getQuantity() + currentQuantityOrdered < quantity)
					JOptionPane.showMessageDialog(null, "There are only " + (currentWarehouse.getQuantity() + currentQuantityOrdered) + " units left in stock!", "Not enough quantity", JOptionPane.WARNING_MESSAGE);
				else {
					//remove from warehouse
					currentWarehouse.setQuantity(currentWarehouse.getQuantity() + currentQuantityOrdered - quantity);
					warehouseService.updateWarehouse(currentWarehouse);
					warehouseView.refreshPanel();
					//place order
					Order order = new Order(id, clientID, productID, quantity);
					orderService.updateOrder(order);
					orderView.refreshPanel();
					Bill bill = new Bill(currentClient, currentProduct, quantity);
					bill.printBill();
				}
			}
			//delete
			if (sourceObject == orderView.getPerformDeleteOrderButton() && orderView.getOrders().getItemCount() != 0) {
				Order order = (Order) orderView.getOrders().getSelectedItem();
				orderService.deleteOrder(order);
				orderView.refreshPanel();
			}
		}
	};
	
	public Controller() {
		clientView = new ClientView(clientActionListener);
		productView = new ProductView(productActionListener);
		warehouseView = new WarehouseView(warehouseActionListener);
		orderView = new OrderView(orderActionListener);
		appView = new AppView(clientView, productView, warehouseView, orderView);
		appView.setVisible(true);
	}
	
	public static void main(String args[]) {
		new Controller();
	}
}
