package models;

public class Client {
	private int ID;
	private String name;
	private String address;
	private String email;
	
	public Client() {
		
	}
	
	public Client(int ID, String name, String address, String email) {
		this.ID = ID;
		this.address = address;
		this.name = name;		
		this.email = email;
	}
	
	public Client(String name, String address, String email) {
		this.address = address;
		this.name = name;		
		this.email = email;
	}

	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setID(int iD) {
		ID = iD;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Overridden method which prints only the ID and name of a client
	 * @return String: ID + name
	 */
	@Override
	public String toString() {
		return "ID" + this.ID + " Name: " + this.name;
	}
}
