package models;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Bill {
	private String text = "";
	
	public Bill(Client client, Product product, int quantity) {
		text += "|----------------------------------------------\n";
		text += "|Client name: " + client.getName() + "\n";
		text += "|Client address: " + client.getAddress() + "\n";
		text += "|Client email: " + client.getEmail() + "\n";
		text += "|----------------------------------------------\n";
		text += "|Product: " + product.getName() + ", price per unit: " + product.getPrice() + ", quantity: " + quantity + "\n";
		text += "|----------------------------------------------\n";
		text += "|TOTAL PRICE: " + quantity * product.getPrice() + "\n";
		text += "|----------------------------------------------\n";
	}

	/**
	 * Prints the the last bill on the current working directory + /bills/LastBill.txt
	 * 
	 */
	public void printBill() {
		String path = "";
		try {
			path = new File(".").getCanonicalPath() + "\\bills";
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File file = new File(path + "\\LastBill.txt");
		
	    BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(text);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
