package models;

public class Product {
	private int ID;
	private String name;
	private int price;
	
	public Product() {
		
	}
	
	public Product(String name, int price) {
		this.price = price;
		this.name = name;
	}
	
	public Product(int ID, String name, int price) {
		this.ID = ID;
		this.price = price;
		this.name = name;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}
	
	@Override
	public String toString() {
		return "ID" + this.ID + " Name: " + this.name;
	}
}