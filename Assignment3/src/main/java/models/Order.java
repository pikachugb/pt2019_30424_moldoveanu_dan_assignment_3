package models;

public class Order {
	private int ID;
	private int clientID;
	private int productID;
	private int quantity;
	
	public Order() {
		
	}
	
	public Order(int ID, int clientID, int productID, int quantity) {
		this.ID = ID;
		this.clientID = clientID;
		this.productID = productID;
		this.quantity = quantity;
	}
	
	public Order(int clientID, int productID, int quantity) {
		this.clientID = clientID;
		this.productID = productID;
		this.quantity = quantity;
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int iD) {
		ID = iD;
	}
	
	public int getClientID() {
		return clientID;
	}
	
	public void setClientID(int clientID) {
		this.clientID = clientID;
	}
	
	public int getProductID() {
		return productID;
	}
	
	public void setProductID(int productID) {
		this.productID = productID;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return "ID" + this.ID + " ClientID: " + this.clientID + " ProductID: " + this.productID;
	}
}
