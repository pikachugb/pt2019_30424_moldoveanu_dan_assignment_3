package models;

public class Warehouse {
	private int ID;
	private int productID;
	private int quantity;
	
	public Warehouse() {
		
	}
	
	public Warehouse(int productID, int quantity) {
		this.productID = productID;
		this.quantity = quantity;
	}
	
	public Warehouse(int ID, int productID, int quantity) {
		this.ID = ID;
		this.productID = productID;
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ID" + this.ID + " ProductID: " + productID;
	}
}
