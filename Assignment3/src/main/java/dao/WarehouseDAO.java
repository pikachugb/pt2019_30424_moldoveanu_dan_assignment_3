package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.Warehouse;

public class WarehouseDAO extends AbstractDAO<Warehouse>{
	private String selectByProductIDQueryString(String field, String condition) {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from warehouse where " + field + " =" + condition);
		return sb.toString();
	}
	
	public Warehouse selectByProductIDQuery(int ID) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = selectByProductIDQueryString("productID",String.valueOf(ID));
		
		try {
			connection = DBConnection.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			if (resultSet.isBeforeFirst())
				return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(resultSet);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
		
		return null;
	}
}
