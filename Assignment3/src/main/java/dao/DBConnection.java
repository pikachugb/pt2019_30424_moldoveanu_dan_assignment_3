package dao;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {
		private static final String driver = "com.mysql.cj.jdbc.Driver";
		private static final String db = "jdbc:mysql://localhost:3306/assignment3?useSSL=false";
		private static final String user = "root";
		private static final String pass = "12345";
		
		
		/**
		 * Contructor for the DBConnection class, which is the class that helps the app to connect to the database
		 * 
		 */
		public DBConnection() {
			try {
				Class.forName(driver);
			}
			catch(ClassNotFoundException e) {
				//System.out.println("Where is your MySQL JDBC Driver?");
				e.printStackTrace();
			}
		}
		
		/**
		 * Returns the connection to the database which will be used in the DAO classes
		 * @throws SQLException - Sqlexception
		 * @return connection - the connection to the database
		 */
	    public static Connection getConnection() throws SQLException {
	    	Connection connection;
		    connection = DriverManager.getConnection(db,user,pass);
		    return connection;
	    }
	    
	    /**
		 * Close the connection to the database, used in the DAO classes
		 * @param connection - Connection to the data base
		 */
	    public static void close(Connection connection) {
		   	try {
			   connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	    }
	    
	    /**
		 * Close the statement, used in the DAO classes
		 * @param statement - the statement of the connection
		 */
	    public static void close(Statement statement) {
		   try {
			   statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	    }
	    
	    /**
		 * Close the resultSet after extracting the data, used in the DAO classes
		 * @param resultSet - the ResultSet of the connection and statement
		 */
	    public static void close(ResultSet resultSet) {
		   try {
			   resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 }
}
