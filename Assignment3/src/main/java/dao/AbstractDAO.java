package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Field;

public class AbstractDAO<T> {
	private Class<T> type;
	
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
    /**
	 * Converts the data from the table into a list of Objects
	 * @param ResultSet
	 * @return List of Objects (Client,Product,Warehouse,Order)
	 */
	protected List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				@SuppressWarnings("deprecation")
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

    /**
	 * Build the String used for selecting the data from tables
	 * @return String - query
	 */
	private String selectAllQueryString() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from ");
		sb.append(type.getSimpleName());
		if (type.getSimpleName().equals("Order"))
			sb.append("s");
		return sb.toString();
	}
	
    /**
	 * Extracts all the data from a table in the database
	 * @return List of Objects
	 */
	public List<T> selectAllQuery() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = selectAllQueryString();
		
		try {
			connection = DBConnection.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(resultSet);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
		
		return null;
	}

    /**
	 * Build the String used for selecting the data from tables by an ID
	 * @param String - the column which you select by 
	 * @param String - the condition of that column
	 * @return String - query
	 */
	private String selectByQueryString(String field, String condition) {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from ");
		sb.append(type.getSimpleName());
		if (type.getSimpleName().equals("Order"))
			sb.append("s");
		sb.append(" where " + field + " =" + condition);
		return sb.toString();
	}

    /**
	 * Extracts the object by an id from a table in the database
	 * @param ID - a unique ID of an Object
	 * @return Object 
	 */
	public T selectByIDQuery(int ID) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = selectByQueryString("ID",String.valueOf(ID));
		
		try {
			connection = DBConnection.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			if (resultSet.isBeforeFirst())
				return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(resultSet);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
		
		return null;
	}
	
    /**
	 * Inserts an Object into a specific table in the database
	 * @param t - the object corresponding to the table
	 */
	public void insertQuery(T t) {
		//Insert into Table(...) values (...)
		StringBuilder sb = new StringBuilder();
		sb.append("insert into ");
		sb.append(type.getSimpleName());
		if (type.getSimpleName().equals("Order"))
			sb.append("s");
		sb.append(" (");
		for (Field field: type.getDeclaredFields()) {
			if (type.getDeclaredFields()[0].equals(field))
				continue;
			
			String f = field.getName();
			sb.append(f + ",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(") values (");
		try {
			for (Field field: type.getDeclaredFields()) {
				if (type.getDeclaredFields()[0].equals(field))
					continue;
				field.setAccessible(true);
				Object valueObject = field.get(t);
				sb.append("\"" + valueObject.toString()+"\",");
			}
		}
		 catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		//conn
		Connection connection = null;
		PreparedStatement statement = null;
		
		try {
			connection = DBConnection.getConnection();
			statement = connection.prepareStatement(sb.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}
	
    /**
	 * Updates an Object into a specific table in the database
	 * @param t - the object corresponding to the table
	 */
	public void updateByIDQuery(T t) {
		StringBuilder sb = new StringBuilder();
		sb.append("update ");
		sb.append(type.getSimpleName());
		if (type.getSimpleName().equals("Order"))
			sb.append("s");
		sb.append(" set ");
		
		try {
			for (Field field: type.getDeclaredFields()) {
				if (type.getDeclaredFields()[0].equals(field))
					continue;
				field.setAccessible(true);
				Object valueObject = field.get(t);
				
				String f = field.getName();
				sb.append(f + "=");
				sb.append("\"" + valueObject.toString()+"\",");
			}
			
			sb.deleteCharAt(sb.length()-1);
			
			Field idField = type.getDeclaredFields()[0];
			idField.setAccessible(true);
			Object valueObject = idField.get(t);
			sb.append(" where id=" + valueObject.toString());
		}
		 catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}


		//conn
		Connection connection = null;
		PreparedStatement statement = null;
		
		try {
			connection = DBConnection.getConnection();
			statement = connection.prepareStatement(sb.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}

    /**
	 * Deletes an Object from a specific table in the database
	 * @param t - the object corresponding to the table
	 */
	public void deleteByIDQuery(T t) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from ");
		sb.append(type.getSimpleName());
		if (type.getSimpleName().equals("Order"))
			sb.append("s");
		
		try {			
			Field idField = type.getDeclaredFields()[0];
			idField.setAccessible(true);
			Object valueObject = idField.get(t);
			sb.append(" where id=" + valueObject.toString());
		}
		 catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		//conn
		Connection connection = null;
		PreparedStatement statement = null;
		
		try {
			connection = DBConnection.getConnection();
			statement = connection.prepareStatement(sb.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}
}
