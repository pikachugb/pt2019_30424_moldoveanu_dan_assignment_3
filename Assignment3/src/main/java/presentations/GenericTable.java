package presentations;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class GenericTable<T> extends JTable{
	private static final long serialVersionUID = 1L;
	private DefaultTableModel model;
	private Class<T> type;
		
	@SuppressWarnings("unchecked")
	public GenericTable() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * Clears this JTable's model
	 * 
	 */
	public void clearTable() {
		model = (DefaultTableModel)this.getModel();
		model.setColumnCount(0);
		model.setRowCount(0);
	}

	/**
	 * Fills this with T's fields as columns and list's Objects as rows
	 * @param list - List of Objects (Client, Product, Warehouse, Order)
	 */
	public void createTable(List<T> list) {
		clearTable();
		for (Field field: type.getDeclaredFields()) {
			model.addColumn(field.getName());
		}
		
		try {
			for (T t: list) {
				String[] tString = new String[10];
				int i = 0;
				for (Field field: type.getDeclaredFields()) {
					field.setAccessible(true);
					Object valueObject = field.get(t);
					tString[i] = valueObject.toString(); 
					i++;
				}
				model.addRow(tString);
			}
		}
		 catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		this.setModel(model);
		this.getColumnModel().getColumn(0).setPreferredWidth(5);
	}
}
